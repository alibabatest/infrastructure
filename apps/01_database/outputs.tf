output "rabbitmq_hostname" {
  value       = "rabbitmq.rabbitmq.svc.cluster.local"
  sensitive   = false
  description = "RabbitMQ Hostname"
}

output "rabbitmq_portname" {
  value       = "5672"
  sensitive   = false
  description = "RabbitMQ Portname"
}

output "rabbitmq_username" {
  value       = "user"
  sensitive   = false
  description = "RabbitMQ Username"
}

output "rabbitmq_password" {
  value       = random_password.rabbitmq.result
  sensitive   = true
  description = "RabbitMQ Password"
}
