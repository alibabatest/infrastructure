terraform {
  required_version = ">= 0.14.0"
  required_providers {
    tls = {
      source  = "hashicorp/tls"
      version = "4.0.4"
    }
    null = {
      source  = "hashicorp/null"
      version = "3.2.1"
    }
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    k8sbootstrap = {
      source  = "nimbolus/k8sbootstrap"
      version = "0.1.3"
    }
  }
}
