resource "kubernetes_namespace" "prometheus" {
  metadata {
    name = "prometheus"
    annotations = {
      name = "prometheus"
    }
  }
}

resource "helm_release" "prometheus" {
  name       = "prometheus"
  namespace  = kubernetes_namespace.prometheus.metadata[0].name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "kube-prometheus"
  version    = "8.22.0"

  values = [yamlencode({})]
}

resource "helm_release" "prometheus_adapter" {
  name       = "prometheus-adapter"
  namespace  = kubernetes_namespace.prometheus.metadata[0].name
  repository = "https://prometheus-community.github.io/helm-charts"
  chart      = "prometheus-adapter"
  version    = "4.7.1"

  values = [yamlencode({
    prometheus = {
      url  = "http://prometheus-kube-prometheus-prometheus.prometheus.svc"
      port = 9090
    }
    rules = {
      custom = [{
        seriesQuery  = "rabbitmq_queue_messages_ready"
        metricsQuery = "sum(<<.Series>>{<<.LabelMatchers>>})"
        name = {
          matches = "^(.*)"
          as      = "messages_waiting_in_queue"
        }
      }]
    }
  })]
}
