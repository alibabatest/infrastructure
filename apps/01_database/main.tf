terraform {
  required_version = ">= 0.14.0"
  required_providers {
    random = {
      source  = "hashicorp/random"
      version = "3.4.3"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
      version = "2.23.0"
    }
    helm = {
      source  = "hashicorp/helm"
      version = "2.11.0"
    }
  }
}

data "terraform_remote_state" "cluster" {
  backend = "local"
  config = {
    path = "../00_cluster/terraform.tfstate"
  }
}

provider "kubernetes" {
  host                   = data.terraform_remote_state.cluster.outputs.host
  cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.ca_crt
  client_certificate     = data.terraform_remote_state.cluster.outputs.client_crt
  client_key             = data.terraform_remote_state.cluster.outputs.client_key
}

provider "helm" {
  kubernetes {
    host                   = data.terraform_remote_state.cluster.outputs.host
    cluster_ca_certificate = data.terraform_remote_state.cluster.outputs.ca_crt
    client_certificate     = data.terraform_remote_state.cluster.outputs.client_crt
    client_key             = data.terraform_remote_state.cluster.outputs.client_key
  }
}
