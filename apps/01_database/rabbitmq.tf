resource "random_password" "rabbitmq" {
  length  = 32
  special = false
}

resource "kubernetes_namespace" "rabbitmq" {
  metadata {
    name = "rabbitmq"
    annotations = {
      name = "rabbitmq"
    }
  }
}

resource "helm_release" "rabbitmq" {
  depends_on = [helm_release.prometheus]

  name       = "rabbitmq"
  namespace  = kubernetes_namespace.rabbitmq.metadata[0].name
  repository = "https://charts.bitnami.com/bitnami"
  chart      = "rabbitmq"
  version    = "12.3.0"

  values = [yamlencode({
    metrics = {
      enabled = true
      serviceMonitor = {
        enabled = true
      }
    }
    auth = {
      username = "user"
      password = random_password.rabbitmq.result
    }
  })]
}
