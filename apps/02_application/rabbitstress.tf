resource "kubernetes_namespace" "rabbitstress" {
  metadata {
    name = "rabbitstress"
    annotations = {
      name = "rabbitstress"
    }
  }
}

resource "kubernetes_secret" "rabbitstress" {
  metadata {
    name      = "regcred"
    namespace = kubernetes_namespace.rabbitstress.metadata[0].name
  }

  type = "kubernetes.io/dockerconfigjson"
  data = {
    ".dockerconfigjson" = jsonencode({
      auths = {
        "registry.gitlab.com" = {
          auth = base64encode("gitlab+deploy-token-3595534:BJxBTQtYuq3ttTt6UtHT")
        }
      }
    })
  }
}

resource "helm_release" "rabbitstress" {
  name       = "rabbitstress"
  namespace  = kubernetes_namespace.rabbitstress.metadata[0].name
  repository = "https://stakater.github.io/stakater-charts"
  chart      = "application"
  version    = "v2.2.12"

  values = [yamlencode({
    deployment = {
      securityContext  = { runAsUser = 999 }
      imagePullSecrets = kubernetes_secret.rabbitstress.metadata[0].name
      image = {
        repository = "registry.gitlab.com/alibabatest/rabbitstress"
        tag        = "1.0.1"
      }
      env = {
        RABBITMQ_URL = {
          value = "amqp://${data.terraform_remote_state.database.outputs.rabbitmq_username}:${data.terraform_remote_state.database.outputs.rabbitmq_password}@${data.terraform_remote_state.database.outputs.rabbitmq_hostname}:${data.terraform_remote_state.database.outputs.rabbitmq_portname}//"
        }
      }
    }
    autoscaling = {
      enabled     = true
      minReplicas = 1
      maxReplicas = 10
      metrics = [{
        type = "External"
        external = {
          metric = {
            name = "messages_waiting_in_queue"
          }
          target = {
            type         = "Value"
            averageValue = 3
          }
        }
      }]
    }
  })]
}
