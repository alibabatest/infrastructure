# Infrastructure

**Infrastructure** is a Terraform application defining the entire organization `infrastructures`

## Installation

Install the Terraform binary on your system:

-   [Terraform CLI](https://www.terraform.io/downloads.html)

Now, use command bellow to install dependencies:

```bash
# Configure terraform secret variables
```

## Usage

```bash
# Goto the layer folder
cd <LAYER>

# Install dependencies
terraform init

# Show changes
terraform plan

# Create infrastructure
terraform apply

# Destroy infrastructure
terraform destroy
```

## Architecture

This terraform project contains three main layer in `apps/` folder, which they are ordered by prefix numbers:

### 00_cluster

This layer is responsible for creating `RKE2` cluster with three masters, the provisioning of cluster was done manually using these commands:

```bash
# Leader
mkdir -p /etc/rancher/rke2/
cat >/etc/rancher/rke2/config.yaml <<EOL
token: my-shared-secret
tls-san:
  - 10.21.110.11
  - 10.21.110.12
  - 10.21.110.13
EOL
curl -sfL https://get.rke2.io | sh -
systemctl enable rke2-server.service
systemctl start rke2-server.service

# Master 1
mkdir -p /etc/rancher/rke2/
cat >/etc/rancher/rke2/config.yaml <<EOL
token: my-shared-secret
server: https://10.21.110.11:9345
tls-san:
  - 10.21.110.11
  - 10.21.110.12
  - 10.21.110.13
EOL
curl -sfL https://get.rke2.io | sh -
systemctl enable rke2-server.service
systemctl start rke2-server.service

# Master 2
mkdir -p /etc/rancher/rke2/
cat >/etc/rancher/rke2/config.yaml <<EOL
token: my-shared-secret
server: https://10.21.110.11:9345
tls-san:
  - 10.21.110.11
  - 10.21.110.12
  - 10.21.110.13
EOL
curl -sfL https://get.rke2.io | sh -
systemctl enable rke2-server.service
systemctl start rke2-server.service
```

### 01_database

This layer will import cluster credentials from `00_cluster` layer and deploy **rabbitmq** and **kube-prometheus** and **prometheus-adapter** in separate namespaces using terraform `helm` and `kubernetes` providers.

### 02_application

This layer will import cluster credentials from `00_cluster` and rabbitmq credentials from `01_database` and deploy **rabbitstress** application using a generic helm chart into a separate namespace.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License

[UNLICENSED]()
